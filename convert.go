package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	scannerID   = "bandit"
	scannerName = "Bandit"
)

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var doc = struct {
		Results []Result `json:"results"`
	}{}

	err := json.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	issues := make([]issue.Issue, len(doc.Results))
	for i, raw := range doc.Results {
		r := RelResult{raw, prependPath}
		issues[i] = issue.Issue{
			Category:    issue.CategorySast,
			Scanner:     scanner,
			Message:     r.IssueText,
			Location:    r.Location(),
			Severity:    severityLevel(r.IssueSeverity),
			Confidence:  confidenceLevel(r.IssueConfidence),
			CompareKey:  r.CompareKey(),
			Identifiers: r.Identifiers(),
		}
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil

}

// RelResult describes a result with a relative path.
type RelResult struct {
	Result
	PrependPath string
}

// Filepath returns the relative path of the affected file.
func (r RelResult) Filepath() string {
	return filepath.Join(r.PrependPath, r.Filename)
}

// CompareKey returns a string used to establish whether two issues are the same.
func (r RelResult) CompareKey() string {
	return strings.Join([]string{r.Filepath(), r.Fingerprint(), r.TestID}, ":")
}

// Result describes a vulnerability found in the source code.
type Result struct {
	Code            string `json:"code"`
	Filename        string `json:"filename"`
	IssueConfidence string `json:"issue_confidence"`
	IssueSeverity   string `json:"issue_severity"`
	IssueText       string `json:"issue_text"`
	LineNumber      int    `json:"line_number"`
	LineRange       []int  `json:"line_range"`
	TestID          string `json:"test_id"`
	TestName        string `json:"test_name"`
}

// Fingerprint calculates the checksum of the affected code.
func (r Result) Fingerprint() string {
	// extract lines without line number and left spaces
	lines := strings.Split(strings.TrimSpace(r.Code), "\n")
	lines2 := make([]string, len(lines))
	for i, line := range lines {
		lines2[i] = strings.TrimSpace(strings.TrimLeft(line, "0123456789"))
	}
	code := strings.Join(lines2, "\n") // code without line numbers

	// create code fingerprint using SHA256
	h := sha256.New()
	h.Write([]byte(code))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// LineEnd returns the last line number of the affected code.
func (r Result) LineEnd() int {
	// LineRange contains all affected lines: [6, 7, 8]
	// so we return the last element
	return r.LineRange[len(r.LineRange)-1]
}

// Location returns a structured Location
func (r RelResult) Location() issue.Location {
	return issue.Location{
		File:      r.Filepath(),
		LineStart: r.LineNumber,
		LineEnd:   r.LineEnd(),
	}
}

// severityLevel converts a Bandit severity to a generic severity level.
// See https://github.com/PyCQA/bandit/blob/1.4.0/bandit/core/constants.py#L23-L25
func severityLevel(s string) issue.SeverityLevel {
	switch s {
	case "HIGH":
		return issue.SeverityLevelHigh
	case "MEDIUM":
		return issue.SeverityLevelMedium
	case "LOW":
		return issue.SeverityLevelLow
	}
	return issue.SeverityLevelUnknown
}

// confidenceLevel converts a Bandit confidence to a generic confidence level.
// See https://github.com/PyCQA/bandit/blob/1.4.0/bandit/core/constants.py#L23-L25
func confidenceLevel(s string) issue.ConfidenceLevel {
	switch s {
	case "HIGH":
		return issue.ConfidenceLevelHigh
	case "MEDIUM":
		return issue.ConfidenceLevelMedium
	case "LOW":
		return issue.ConfidenceLevelLow
	}
	return issue.ConfidenceLevelUnknown
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (r Result) Identifiers() []issue.Identifier {
	return []issue.Identifier{
		r.BanditIdentifier(),
	}
}

// BanditIdentifier returns a structured Identifier for a Bandit TestID
func (r Result) BanditIdentifier() issue.Identifier {
	var documentedIDs = []string{"B101", "B102", "B103", "B104", "B105", "B106", "B107", "B108",
		"B109", "B110", "B111", "B112", "B201", "B501", "B502", "B503", "B504", "B505", "B506", "B601",
		"B602", "B603", "B604", "B605", "B606", "B607", "B608", "B609", "B701", "B702"}
	var url string

	for _, ID := range documentedIDs {
		if ID == r.TestID {
			url = fmt.Sprintf("https://docs.openstack.org/bandit/latest/plugins/%s_%s.html", strings.ToLower(r.TestID), r.TestName)
			break
		}
	}

	return issue.Identifier{
		Type:  "bandit_test_id",
		Name:  fmt.Sprintf("Bandit Test ID %s", r.TestID),
		Value: r.TestID,
		URL:   url,
	}
}
