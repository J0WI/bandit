package main

import (
	"io"
	"os"
	"os/exec"

	"github.com/urfave/cli"
)

const (
	pathBandit = "/usr/local/bin/bandit"
	pathOutput = "/tmp/bandit.json"

	flagExcludedPaths = "exclude"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   flagExcludedPaths,
			Usage:  "comma-separated list of paths (glob patterns supported) to exclude from scan",
			EnvVar: "SAST_BANDIT_EXCLUDED_PATHS",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	exPaths := c.String(flagExcludedPaths)
	cmd := exec.Command(pathBandit, "-a", "vuln", "-f", "json", "--exclude", exPaths, "-o", pathOutput, "-r", ".")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// NOTE: The error returned by bandit is not relevant.
	// Bandit returns 1 whenever a vulnerability has been found
	// but it returns 0 when there's no vulnerability
	// or when the path given to -r does not exist.
	cmd.Run()
	return os.Open(pathOutput)
}
