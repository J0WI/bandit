package main

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestConvert(t *testing.T) {
	in := `{
  "results": [
    {
      "code": "58             )\n59             context['content'] = mark_safe(content)\n60         return context\n",
      "filename": "tmp/py-app/cms/cms_plugins.py",
      "issue_confidence": "HIGH",
      "issue_severity": "MEDIUM",
      "issue_text": "Use of mark_safe() may expose cross-site scripting vulnerabilities and should be reviewed.",
      "line_number": 59,
      "line_range": [
        59
      ],
      "test_id": "B308",
      "test_name": "blacklist"
		},
		{
      "code": "830     def mark_descendants_pending(self, language):\n831         assert self.publisher_is_draft\n832 \n833         descendants = self.get_descendants().filter(\n",
      "filename": "tmp/py-app/cms/models/pagemodel.py",
      "issue_confidence": "HIGH",
      "issue_severity": "LOW",
      "issue_text": "Use of assert detected. The enclosed code will be removed when compiling to optimised byte code.",
      "line_number": 831,
      "line_range": [
        831,
        832
      ],
      "test_id": "B101",
      "test_name": "assert_used"
    }
  ]
}`

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	r := strings.NewReader(in)
	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{

				Category:   issue.CategorySast,
				Message:    "Use of mark_safe() may expose cross-site scripting vulnerabilities and should be reviewed.",
				CompareKey: "app/tmp/py-app/cms/cms_plugins.py:3cec1ad7dbb0c4e3aadaa528a3ba3a998f558271046e748d45bd31f73ae7d602:B308",
				Severity:   issue.SeverityLevelMedium,
				Confidence: issue.ConfidenceLevelHigh,
				Scanner:    scanner,
				Location: issue.Location{
					File:      "app/tmp/py-app/cms/cms_plugins.py",
					LineStart: 59,
					LineEnd:   59,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "bandit_test_id",
						Name:  "Bandit Test ID B308",
						Value: "B308",
					},
				},
			},
			{
				Category:   issue.CategorySast,
				Message:    "Use of assert detected. The enclosed code will be removed when compiling to optimised byte code.",
				CompareKey: "app/tmp/py-app/cms/models/pagemodel.py:5b8903fbc5b073ec09232c7536feac20906b8589f30278169d7b4f02108d8857:B101",
				Severity:   issue.SeverityLevelLow,
				Confidence: issue.ConfidenceLevelHigh,
				Scanner:    scanner,
				Location: issue.Location{
					File:      "app/tmp/py-app/cms/models/pagemodel.py",
					LineStart: 831,
					LineEnd:   832,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "bandit_test_id",
						Name:  "Bandit Test ID B101",
						Value: "B101",
						URL:   "https://docs.openstack.org/bandit/latest/plugins/b101_assert_used.html",
					},
				},
			},
		},
		DependencyFiles: []issue.DependencyFile{},
		Remediations:    []issue.Remediation{},
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
