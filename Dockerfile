FROM python:3-stretch

ARG BANDIT_VERSION=1.6.2

RUN pip install bandit==$BANDIT_VERSION
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
