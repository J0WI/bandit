# Bandit analyzer changelog

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!20)

## v2.3.0
- Add support for custom CA certs (!17)

## v2.2.2
- Use Debian Stretch as base image (!14)

## v2.2.1
- Update bandit version to 1.6.2

## v2.2.0
- Add `SAST_BANDIT_EXCLUDED_PATHS` option to exclude paths from scan
- Add `SAST_EXCLUDED_PATHS` option to exclude paths from report (common v2.3.0)
- Report "confirmed" confidence instead of "critical" (common v2.2.0)

## v2.1.1
- Update common to v2.1.6

## v2.1.0
- Update bandit version to 1.5.1

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
